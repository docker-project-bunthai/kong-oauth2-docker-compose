package com.example.data.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("data")
public class DataRestController {
    @GetMapping
    public String getData() {
        return "Hello data!";
    }
}
