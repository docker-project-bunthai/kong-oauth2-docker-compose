_Make sure your `docker-compose` is `3.8+`, if producing any errors, you need to update_

# Create Network
`
$ docker network create --gateway 172.18.0.1 --subnet 172.18.0.0/16 test
`

# Start Postgres
`
$ docker-compose up -d postgres
`

# Konga GUI
 - Access Konga UI: http://0.0.0.0:1337 <br>
 - Konga connects to Kong admin URL: http://kong:8001

# Add Service
**`data-service` will point to an api endpoint (http://data-service:8080)** <br>
````
// For data-service
$ curl -i -X POST \
    --url http://localhost:8001/services/ \
    --data 'name=data-service' \
    --data 'url=http://data-service:8080'

// For account-service
$ curl -i -X POST \
    --url http://localhost:8001/services/ \
    --data 'name=account-service' \
    --data 'url=http://account-service:8080'

// For OAuth2
$ curl -i -X POST \
    --url http://localhost:8001/services/ \
    --data 'name=oauth2' \
    --data 'url=http://account-service:8080'
````

# Add Route by Hosts
````
$ curl -i -X POST \
    --url http://localhost:8001/services/data-service/routes \
    --data 'hosts[]=data'
````

# Add Route by Paths
- `strip_path=false, default true`
- request from a client http://localhost:8000/data

````
/**
* if strip_path=false, it will exclude `paths[]=/data` in endpoints
* for example, http://localhost:8000/data/data => http://localhost:8000/data
*/

$ curl -i -X POST \
    --url http://localhost:8001/services/data-service/routes \
    --data 'name=dataAPI' \
    --data 'paths[]=/data' \
    --data 'strip_path=false'

$ curl -i -X GET \
    --url http://localhost:8000/data
````


# Add Route by Paths
- `strip_path=true, default true`
- request from client http://localhost:8000/data/data
- 1st data path is kong's path
- 2nd data path is the rest controller's endpoint

````

/**
* if strip_path=false, it will include `paths[]=/data` in endpoints
* for example, http://localhost:8000/data/data
*/

$ curl -i -X POST \
    --url http://localhost:8001/services/data-service/routes \
    --data 'name=dataAPI' \
    --data 'paths[]=/data' \
    --data 'strip_path=true'

$ curl -i -X GET \
    --url http://localhost:8000/data/data
````

# Add Route by Paths
- `strip_path=true`
- request from client http://localhost:8000/v1/data
- 1st v1 path is kong's path
- 2nd data path is the rest controller's endpoint

````
/**
* if strip_path=true, it will include `paths[]=/v1` in endpoints
* for example, http://localhost:8000/v1/data => http://localhost:8000/v1/data
*/

// For data-service
$ curl -i -X POST \
  --url http://localhost:8001/services/data-service/routes \
  --data 'name=dataAPI' \
  --data 'paths[]=/v1' \
  --data 'strip_path=true'

  $ curl -i -X GET \
  --url http://localhost:8000/v1/data

// For account-service
$ curl -i -X POST \
    --url http://localhost:8001/services/account-service/routes \
    --data 'name=accountAPI' \
    --data 'paths[]=/data' \
    --data 'strip_path=false'

$ curl -i -X GET \
    --url http://localhost:8000/data/data
````


# Add Route by Methods
- `default strip_path=true`
- request from client http://localhost:8000/data

######Add `data-service` route
````
$ curl -i -X POST \
    --url http://localhost:8001/services/data-service/routes \
    --data 'name=dataAPI' \
    --data 'methods[]=GET'

$ curl -i -X GET \
    --url http://localhost:8000/data
````

######Add all `account-service` routes
````
$ curl -i -X POST \
    --url http://localhost:8001/services/account-service/routes \
    --data 'name=accountAPI' \
    --data 'strip_path=false' \
    --data 'paths[]=/user/myToken' \
    --data 'paths[]=/user/myToken/hello' \
    --data 'paths[]=/user/myToken/hello/free' \
    --data 'methods[]=POST' \
    --data 'methods[]=GET'
````

######Add `account-service` routes which are free to access
````
$ curl -i -X POST \
    --url http://localhost:8001/services/account-service/routes \
    --data 'name=free' \
    --data 'strip_path=false' \
    --data 'paths[]=/user/myToken' \
    --data 'paths[]=/user/myToken/hello/free' \
    --data 'methods[]=POST' \
    --data 'methods[]=GET'
````

######Add `account-service` routes which are restrict to authenticate
````
$ curl -i -X POST \
    --url http://localhost:8001/services/account-service/routes \
    --data 'name=auth' \
    --data 'strip_path=false' \
    --data 'paths[]=/user/myToken/hello' \
    --data 'methods[]=POST' \
    --data 'methods[]=GET'
````

######Add Route for OAuth2 Service
````
$ curl -i -X POST \
    --url http://localhost:8001/services/oauth2/routes \
    --data 'name=oauth2token' \
    --data 'strip_path=true' \
    --data 'paths[]=/v1' \
    --data 'methods[]=POST'
````

# Add OAuth2 Plugins to Services

######Add oauth2 plugin to `data-service`

````
$ curl -X POST http://localhost:8001/services/data-service/plugins \
    --data "name=oauth2" \
    --data "config.enable_password_grant=true" \
    --data "config.accept_http_if_already_terminated=true" \
    --data "config.global_credentials=true"
````

######Add oauth2 plugin to `account-service`

````
curl -X POST http://localhost:8001/services/account-service/plugins \
    --data "name=oauth2" \
    --data "config.enable_password_grant=true" \
    --data "config.accept_http_if_already_terminated=true" \
    --data "config.global_credentials=true"
````

######Add oauth2 plugin to `oauth2` service

````
curl -X POST http://localhost:8001/services/oauth2/plugins \
    --data "name=oauth2" \
    --data "config.enable_password_grant=true" \
    --data "config.accept_http_if_already_terminated=true" \
    --data "config.global_credentials=true"
````

######**\*Add OAuth2 plugin apply to all Entry point all entry points**
````
curl -X POST http://localhost:8001/plugins \
    --data "name=oauth2" \
    --data "config.enable_password_grant=true" \
    --data "config.accept_http_if_already_terminated=true" \
    --data "config.global_credentials=true"
````

# Add OAuth2 Plugin to Routers
````
curl -X POST http://localhost:8001/routes/auth/plugins \
    --data "name=oauth2" \
    --data "config.enable_password_grant=true" \
    --data "config.accept_http_if_already_terminated=true" \
    --data "config.global_credentials=true"
````

# Add Consumer
- use the response id: `d3506ca1-2747-4c80-9efe-b1c2998a6631`
````
curl -X POST http://localhost:8001/consumers/ \
    --data "username=hoho1" \
    --data "custom_id=haha1"
````

# Add Application
- The above consumerId: `d3506ca1-2747-4c80-9efe-b1c2998a6631`

````
curl -X POST http://localhost:8001/consumers/hoho1/oauth2 \
    --data "name=TestApplication" \
    --data "client_id=haha1" \
    --data "client_secret=haha1-secret" \
    --data "redirect_uris=https://www.pi-pe.co.jp/"
````

# Access Token

Endpoint to request:
- curl -X GET http://localhost:8000/v1/oauth2/token \
- curl -X GET http://localhost:8000/user/myToken/oauth2/token \
- curl -X GET http://localhost:8000/data/oauth2/token \
It depends on the services you add and `strip_path=true/false`.

_**I would like to recommend adding oauth2 plugin to all services 
then you probably can access oauth2 token to all the endpoint above**_ 

````

/**
* - username,password are optional, but they are used to validate by ourself before issuing a token
* - authenticated_userid(our user id) will be store in kongdb => oauth2_token table
* - in `curl` you can use both `--data` and `--form`
*/

/**
* Requesting from the internal service such RestTemplate, or Webclient
* - in docker-compose KONG_TRUSTED_IPS: 0.0.0.0/0 # 0.0.0.0/0, ::0 
*/
$ curl -X POST http://localhost:8000/v1/oauth2/token \
    --header "X-Forwarded-Proto=https" \
    --data "provision_key=Xzna53PPFvBTAb47DzSTYDBbkAaNrac0" \
    --data "client_id=haha1" \
    --data "client_secret=haha1-secret" \
    --data "grant_type=password" \
    --data "authenticated_userid=1" \
    --data "username=hoho1" \
    --data "password=123"

/**
* Requesting from clients such Postman
* - in docker-compose ports: 8443:8443 is required to allow to access from the clients
* - it's suggested to allow only localhost 127.0.0.1 not 0.0.0.0 which is can be bined to public access
*/
$ curl --location --request POST 'https://localhost:8443/v1/oauth2/token' \
    --insecure \
    --data 'grant_type=password' \
    --data 'client_id=haha1' \
    --data 'client_secret=haha1-secret' \
    --data 'provision_key=PLTmcWrfON5KLVWw6s82XIgsWOQIwMOI' \
    --data 'authenticated_userid=1'

or

$ curl --location --request POST 'https://localhost:8443/v1/oauth2/token' \
    --insecure \
    --form 'grant_type=password' \
    --form 'client_id=haha1' \
    --form 'client_secret=haha1-secret' \
    --form 'provision_key=PLTmcWrfON5KLVWw6s82XIgsWOQIwMOI' \
    --form 'authenticated_userid=1'

````

# Refresh Token

````
curl --location --request POST 'https://localhost:8443/v1/oauth2/token' \
    --form 'grant_type="refresh_token"' \
    --form 'client_id="haha1"' \
    --form 'client_secret="haha1-secret"' \
    --form 'refresh_token="5TF6M2gFhGSUH43k8prLgLfSv9CevYS6"'
````

# To Access Token
**_Note, you must add oauth2 plugin to a service whose strip is true, `paths[] = /v1` or to all services, <br>
then you can access to the endpoint `https://localhost:8443/v1/oauth2/token`_** 
###### Request from Postman
- You must add in docker-compose `kong => ports => 8443:8443`, it will expose an url `https://localhost:8443/v1/oauth2/token`
- Internal services(RestTemplate, Webclient) request the token `http://kong:8000/v1/oauth2/token` and `X-Forwarded-Proto => https`

# Applying Service, Route, Plugin Process
If we check all the above documents, it's a little complex. <br>
Just go through the below document, it will be summary and stick to the points.

###### Create Kong Service
- `--data 'url=http://kong:8000/user'`, when you access to `api gateway localhost:8000`,  
it will automatically add for you `localhost:8000/user`. <br>
- `/user` will be still present for its own request life's time, 
while `/v1` is used only when requesting to `localhost:8000/v1` 
- `/user` will be included when searching for the matched end points.

````
// For User
$ curl -i -X POST \
    --url http://localhost:8001/services/ \
    --data 'name=user-api-gateway' \
    --data 'url=http://kong:8000/user'
````

###### Create Kong Route
- Add `/v1` to `user-api-gateway` service.
So, you have to added `/v1` when requesting, `http://localhost:8000/v1`.
- `/v1` required only when requesting `api gateway localhost:8000/v1`.
- `/v1` will be excluded when searching for the matched end points. 
````
$ curl -i -X POST \
    --url http://localhost:8001/services/user-api-gateway/routes \
    --data 'name=userapi' \
    --data 'strip_path=true' \
    --data 'paths[]=/v1'
````

###### Just make it simple instead of the both above cases
I prefer this way. <br>
# Create User API

````
========================================================================
// Add OAuth2 Service In Account-Service
$ curl -i -X POST \
    --url http://localhost:8001/services/ \
    --data 'name=oauth2service' \
    --data 'url=http://account-service:8080'

// Add OAuth2 access token endpoint
$ curl -i -X POST \
    --url http://localhost:8001/services/oauth2service/routes \
    --data 'name=oauth2api' \
    --data 'strip_path=true' \
    --data 'paths[]=/v1'
==============================================

OR 

==============================
$ curl -i -X POST \
    --url http://localhost:8001/services/ \
    --data 'name=oauth2-service' \
    --data 'url=http://kong:8000'

 curl -i -X POST \
    --url http://localhost:8001/services/oauth2-service/routes \
    --data 'name=oauth2api' \
    --data 'strip_path=true' \
    --data 'paths[]=/v1'
============================ 

// Add Account-Service
$ curl -i -X POST \
    --url http://localhost:8001/services/ \
    --data 'name=account-service' \
    --data 'url=http://account-service:8080'

// Add Account Service Route
$ curl -i -X POST \
    --url http://localhost:8001/services/account-service/routes \
    --data 'name=accountUserApi.accountService' \
    --data 'strip_path=false' \
    --data 'paths[]=/user/myToken' \
    --data 'paths[]=/user/myToken/hello' \
    --data 'paths[]=/user/myToken/hello/free'

````

###### **\*Add OAuth2 Plugin to Kong Route**
Every end points which are started by `/v1` will be authenticated by OAuth2. <br>
Because we already added the route `/v1` in `user-api-gateway` which belongs to `url=http://kong:8000`
````
$ curl -X POST http://localhost:8001/services/oauth2service/plugins \
    --data "name=oauth2" \
    --data "config.enable_password_grant=true" \
    --data "config.accept_http_if_already_terminated=true" \
    --data "config.global_credentials=true"

$ curl -X POST http://localhost:8001/routes/accountUserApi.accountService/plugins \
    --data "name=oauth2" \
    --data "config.enable_password_grant=true" \
    --data "config.accept_http_if_already_terminated=true" \
    --data "config.global_credentials=true"
````

# Key Authentication
We will add the Key Authentication plugin to `/sys` endpoints

###### Add System Service
````
// Add Kong Service for System API
$ curl -i -X POST \
    --url http://localhost:8001/services/ \
    --data 'name=system-api-gateway' \
    --data 'url=http://kong:8000'

````

````
// Add System Route
$ curl -i -X POST \
    --url http://localhost:8001/services/system-api-gateway/routes \
    --data 'name=systemapi' \
    --data 'strip_path=true' \
    --data 'paths[]=/sys' 

````

###### Add System API
````

// Add Account-Service
$ curl -i -X POST \
    --url http://localhost:8001/services/ \
    --data 'name=account-service' \
    --data 'url=http://account-service:8080'

// Add System Endpoints to Account Service Route
$ curl -i -X POST \
    --url http://localhost:8001/services/account-service/routes \
    --data 'name=accountSysApi.accountService' \
    --data 'strip_path=false' \
    --data 'paths[]=/sys/accounts'
````

###### Add `key-auth` plugin to System

````
// Apply for service
curl -X POST http://localhost:8001/services/system-api-gateway/plugins \
    --data "name=key-auth"

// apply for route
curl -X POST http://localhost:8001/routes/accountSysApi.accountService/plugins \
    --data "name=key-auth" 
```` 

###### Add Consumer for System
````
curl -X POST http://localhost:8001/consumers/ \
    --data "username=hoho2" \
    --data "custom_id=haha2"
````

###### Create a Key

````
// Create key for consumer
curl -X POST http://localhost:8001/consumers/hoho2/key-auth -d 'key=sys_key'
````

###### Access API

````
curl --location --request POST 'http://localhost:8000/sys/accounts' \
--header 'apiKey: sys_key' \
--header 'Content-Type: application/json' \
-d '{}'
````

 
    
 
 