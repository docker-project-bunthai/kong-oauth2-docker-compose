package com.example.account.api;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("sys/accounts")
public class SystemAccountRestController {

    @PostMapping
    public Map create(
        @RequestBody Map accountForm
        ){
        return accountForm;
    }

}
