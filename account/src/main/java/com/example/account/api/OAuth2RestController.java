package com.example.account.api;

import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("user/myToken")
public class OAuth2RestController {

    @PostMapping("hello")
    public String hello(){
        return "Hello";
    }

    @PostMapping("hello/free")
    public String helloFree(){
        return "Hello Free";
    }

    @PostMapping
    public Map token(
            @RequestBody Map<String, String> oauthForm
    ){

        System.out.println("ddddddddddddddddd");

//        Not sure why this url is not working?
//        String url = "https://kong:8443/v1/oauth2/token";
        String url = "http://kong:8000/v1/oauth2/token";

        HttpHeaders headers = new HttpHeaders();
        headers.add("X-Forwarded-Proto", "https");

        Map<String, String> param = new HashMap<>();
        param.put("provision_key", "ZVW7OBoh3pqGxMIHuaGtX0QIx0UGMGDb");
        param.put("client_id", "haha1");
        param.put("client_secret", "haha1-secret");
        param.put("authenticated_userid", "1");
        param.put("grant_type", "password");

        HttpEntity<Map<String, String>> httpEntity = new HttpEntity<>(param, headers);

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<Map> response = restTemplate.postForEntity(url, httpEntity, Map.class);

        System.out.println(response);

        return response.getBody();
    }

}
